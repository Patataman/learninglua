# -*- coding: utf-8 -*-

import csv, random
from os import mkdir
#DATOS DE LOS FICHEROS DE ENTRADA

datos = []
datosValidacion = []
#razón de aprendizaje
razonDeAprendizaje = 0.005
nombre = ""+str(razonDeAprendizaje)+"_3000"
mkdir(nombre)
#FICHEROS PARA ENTRENAR
csvFile = open('datosEntrenamiento.csv','r')
csvTraining = csv.reader(csvFile, delimiter=',')
csvFile = open('datosValidacion.csv','r')
csvValidacion = csv.reader(csvFile, delimiter=',')

#FICHEROS DE SALIDA
entrenamiento_validacionFile = open(nombre+'/errorres_E_V.csv', 'w')
pesosUmbralFile = open(nombre+'/pesos_umbral.csv', 'w')
salidasTestFile = open(nombre+'/salidasRed.csv', 'w')

#Recuperar datos
for fila in csvTraining:
	#Recuperar datos
	datos.append(fila)

#Recuperar datos
for fila in csvValidacion:
	#Recuperar datos
	datosValidacion.append(fila)

pesos = []

#Inicializar pesos aleatorios Paso 1
for i in range(0,len(datos[0])):
	pesos.append(float(random.random()))

#RANGE NO LLEGA HASTA EL 2º VALOR
#Aprendizaje
errorCuadraticoValidacionN_1 = float('Inf')
salir = 0
for i in range(0,3000): #Paso 7
	errorCuadratico = 0.0

	for instancia in datos: #Paso 6 y Paso 2
		#Valor esperado (último de la fila) Paso 3
		valorEsperado = float(instancia[72])

		y = 0.0
		#Por cada Xi hasta Xn-2 (Hay que ir de 0 hasta N-2 porque N-1 es la clase)
		for Xi in range(0,72):
			#Se calcula el sumatorio
			y += float(instancia[Xi])*pesos[Xi]
			#print float(instancia[Xi])

		#Se le suma el umbral
		y += pesos[72]
		#Aqui se tiene el valor obtenido y hay que compararlo con el deseado
		diferencia = valorEsperado - y
		#Actualizar pesos Pasos 4 y 5
		for Wi in range(0,72):
			pesos[Wi] += razonDeAprendizaje * diferencia * float(instancia[Wi])
		#Actualizar umbral
		pesos[72] += razonDeAprendizaje * diferencia


	#Se calcula el error de validación
	errorCuadraticoValidacion = 0.0
	for instancia in datos:
		#Valor esperado
		valorEsperado = float(instancia[72])

		#Valor predicho
		y = 0.0
		for Xi in range(0,72):
			#Se calcula el sumatorio
			y += float(instancia[Xi])*pesos[Xi]
		#Se le suma el umbral
		y += pesos[72]
		#Error
		diferencia = valorEsperado - y
		errorCuadratico += diferencia*diferencia

	errorCuadratico = errorCuadratico/len(datos)

	#Se calcula el error de validación
	errorCuadraticoValidacion = 0.0
	for instancia in datosValidacion:
		#Valor esperado
		valorEsperadoValidacion = float(instancia[72])

		#Valor predicho
		y = 0.0
		for Xi in range(0,72):
			#Se calcula el sumatorio
			y += float(instancia[Xi])*pesos[Xi]
		#Se le suma el umbral
		y += pesos[72]
		#Error
		diferenciaValidacion = valorEsperadoValidacion - y
		errorCuadraticoValidacion += diferenciaValidacion*diferenciaValidacion

	errorCuadraticoValidacion = errorCuadraticoValidacion/len(datosValidacion)

	#Si el error actual es mayor que el error actual con una diferencia significativa, se
	#toma en cuenta
	if errorCuadraticoValidacionN_1/errorCuadraticoValidacion > 1.2:
		salir += 1
		if salir == 10:
			print "Se sale al ciclo {0}".format(i)
			break
	errorCuadraticoValidacionN_1 = errorCuadraticoValidacion

	#Se guarda el ciclo, error entrenamiento y error validación en el fichero
	entrenamiento_validacionFile.write(str(i)+';'+str(errorCuadratico)+';'+str(errorCuadraticoValidacion)+'\n')

print "Error de entrenamiento {0} \t Error de validación {1}".format(errorCuadratico, errorCuadraticoValidacion)

#Se guarda Umbral y Pesos en el fichero
for peso in pesos:
	pesosUmbralFile.write(str(peso)+';')


csvFile = open('datosTest.csv','r')
csvTest = csv.reader(csvFile, delimiter=',')
datosTest = []
for fila in csvTest:
	#Recuperar datos
	datosTest.append(fila)

#Se repite el proceso de calcular error, pero esta vez sólo para test
errorCuadratico = 0.0
#Se calcula el error del archivo de test
for instancia in datosTest: #Paso 6 y Paso 2
	#Valor esperado (último de la fila) Paso 3
	valorEsperado = float(instancia[72])

	y = 0.0
	#Por cada Xi hasta Xn-2 (Hay que ir de 0 hasta N-2 porque N-1 es la clase)
	for Xi in range(0,72):
		#Se calcula el sumatorio
		y += float(instancia[Xi])*pesos[Xi]
		#print float(instancia[Xi])

	#Se le suma el umbral
	y += pesos[72]
	#Se guarda la salida esperada para cada instancia de Test
	salidasTestFile.write(str(y)+';')

	#Aqui se tiene el valor obtenido y hay que compararlo con el deseado
	diferencia = valorEsperado - y
	errorCuadratico += diferencia*diferencia

#Error obtenido en Test
errorCuadratico = errorCuadratico/len(datosTest)
print "Error en test: {0}".format(errorCuadratico)

