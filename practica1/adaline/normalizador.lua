
require 'io'

function normalizar(lista)
  listaAux = {}
  min = 0
  max = 0

  for cont=1, 73 do
    --Aqui se guardan los valores y se encuentra el min y el max
    for key,value in ipairs(lista) do
      if key == 2 then
        min = tonumber(lista[key][cont])
        max = tonumber(lista[key][cont])
      end
      listaAux[key] = tonumber(lista[key][cont])
      if key ~= 1 then
        if listaAux[key] < min then
          min = listaAux[key]
        end
        if listaAux[key] > max then
          max = listaAux[key]
        end
      end
    end
    --Aqui se normaliza
    for key,value in ipairs(lista) do
      if key >= 2 then
        listaAux[key] = (listaAux[key]-min)/(max-min)
        lista[key][cont] = listaAux[key]
      end
    end
    --lista[key] = listaAux
  end
end

-- split a string
function string:split(delimiter)
  local result = { }
  local from  = 1
  local delim_from, delim_to = string.find( self, delimiter, from  )
  while delim_from do
    table.insert( result, string.sub( self, from , delim_from-1 ) )
    from  = delim_to + 1
    delim_from, delim_to = string.find( self, delimiter, from  )
  end
  table.insert( result, string.sub( self, from  ) )
  return result
end
-------------------------

local filename = "datos_sotavento.csv"
local normalizado = "datos_sotavento_normalizados.csv"

filefilename = io.open(filename,"r")
fileNormal = io.open(normalizado,"w")

lista = {}

i = 1
for line in filefilename:lines() do
  line = string.gsub(line,',','.')
	aux = string.split(line, ";")
  lista[i] = {}
  for key, valor in ipairs(aux) do
    lista[i][key] = valor
  end
  i = i + 1
end

-- tenemos la lista en memoria
normalizar(lista)

for key, valor in ipairs(lista) do
  for key2, valor2 in ipairs(valor) do
    fileNormal:write(valor2)
    if key2 ~= 73 then
      fileNormal:write(';')
    end
  end
  fileNormal:write('\n')
end

--[[for key, valor in ipairs(lista) do
  for pepe=1, #valor do
    print(valor[pepe])
  end
]]--end
--TABLA DE TABLAS, OH DIOS MATAME

--a = 3
--for line in filefilename:lines() do
--	if a == 3 or a == 2 then
--		h = string.split(line, ";")
--		for key, valor in ipairs(h) do
--			print (valor)
--			fileNormal:write(valor .. " \n")  
--		end
--
--	end
--	if a == 3 then
--		a = 2
--	else
--		a = 0	
--	end
--end



filefilename:close() 
fileNormal:close()

--local file = io.open("filename.txt", "w")
--
--  file:write("Linea ")  
-- 
-- file:close()  