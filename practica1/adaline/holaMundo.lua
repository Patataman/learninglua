require 'table'

fredSum = 0
--Begin, End [, step]
--for j = 100, 1, -1 do 
--	fredSum = 1 + fredSum
--	io.write(fredSum..'\n')
--end

table = {pos1=1, pos2=2} -- TABLA 
table.pos3 = 3
print(table)

-- bucle foreach
--[[
 	for clave, valor in ipairs(table) do
 		end
]]--
io.write("FOREACH\n")
-- pairs recorre tablas de forma aleatoria (desconozco como hacerlo normal)
-- aunque por algún motivo las recorre en orden inverso :/
for key, val in pairs(table) do  -- Table iteration.
  print(key, val)
end
--ipairs es sólo válido para arrays normales
array2 = {1,2,45,2}
for key, val in ipairs(array2) do
	print(val)
end

-- bucle for a lo java
--[[
	for contador, #array do   // Se usa # porque indica el tamaño
		end
]]--
io.write("FOR JAVA\n")
array = {"Numero1", 123, "cosasnazis",4}
for cont=1, #array do
	print(array[cont])
end
-------- LOS ARRAY EMPIEZAN EN 1 !!!!!!!!!!!!!!!! EN 1 OMFG !!!!!!!!!!!!!!! -----------------

io.write("Se concatenan strings" .. " mediante " .. " ..\n")


arrayInArray = {pos1={1,2,3,4}, pos2={5,6,7,8}, pos3={9,10,11,12}}
for key, arrayIn in ipairs(arrayInArray) do
	for cont=1, #arrayIn do
		print(arrayIn[cont]..' ')
	end
	print('\n')
end

tabla = {1,2}
print(#tabla)

print(#arrayInArray) --Devuelve 0, no vale para tablas, sólo para arrays

-- TODAS LAS TABLAS DEBERÍAN GENERARSE HACIENDO USO DE LA LIBRERÍA table
-- http://www.lua.org/pil/19.1.html