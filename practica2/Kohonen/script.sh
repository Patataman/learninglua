#
# $1 dimensión X ---Default: 10
# $2 Dimensión Y ---Default: 10
# $3 ciclos de entrenamiento ---Default: 1000
# $4 ratio de aprendizaje ---Default: 0.05
# $5 radio de vecindad  ---Default: 10
# $6 ratio de aprendizaje para ajustar ---Default: 0.02
# $7 radio de vecindad para ajustar  ---Default: 3
# $8 topologia: hexa y rect ---Default: hexa
# $9 tipo de vecindad: bubble o gaussian ---Default: bubble

/opt/som_pak-3.1/randinit -din trainNormalizado.dat -cout MapaInicial.cod -xdim $1 -ydim $2 -topol $8 -neigh $9
/opt/som_pak-3.1/vsom -din trainNormalizado.dat -cin MapaInicial.cod -cout MapaEntrenado.cod -rlen $3 -alpha $4 -radius $5
/opt/som_pak-3.1/vsom -din trainNormalizado.dat -cin MapaEntrenado.cod -cout MapaEntrenado.cod -rlen $3 -alpha $6 -radius $7
/opt/som_pak-3.1/qerror -din testNormalizado.dat -cin MapaEntrenado.cod > x$1_y$2_$8_$9_t$3_r$4_v$5_trazaQError_test.txt
/opt/som_pak-3.1/qerror -din trainNormalizado.dat -cin MapaEntrenado.cod > x$1_y$2_$8_$9_t$3_r$4_v$5_trazaQError_train.txt
/opt/som_pak-3.1/vcal -din trainNormalizado.dat -cin MapaEntrenado.cod -cout MapaCalibrado.cod
/opt/som_pak-3.1/visual -din testNormalizado.dat -cin MapaEntrenado.cod -dout x$1_y$2_$8_$9_t$3_r$4_v$5_SalidaVisual_test.txt
/opt/som_pak-3.1/visual -din testNormalizado.dat -cin MapaCalibrado.cod -dout x$1_y$2_$8_$9_t$3_r$4_v$5_SalidaVisualCal_test.txt
/opt/som_pak-3.1/visual -din trainNormalizado.dat -cin MapaEntrenado.cod -dout x$1_y$2_$8_$9_t$3_r$4_v$5_SalidaVisual_train.txt
/opt/som_pak-3.1/visual -din trainNormalizado.dat -cin MapaCalibrado.cod -dout x$1_y$2_$8_$9_t$3_r$4_v$5_SalidaVisualCal_train.txt
/opt/som_pak-3.1/sammon -cin MapaEntrenado.cod -cout proyeccion.sam -rlen $3 -ps
/opt/som_pak-3.1/umat -cin MapaEntrenado.cod > grMapa.ps
/opt/som_pak-3.1/umat -cin MapaCalibrado.cod > grMapaCal.ps

mkdir x$1_y$2_$8_$9_t$3_r$4_v$5
mv MapaInicial.cod x$1_y$2_$8_$9_t$3_r$4_v$5
mv MapaEntrenado.cod x$1_y$2_$8_$9_t$3_r$4_v$5
mv MapaCalibrado.cod x$1_y$2_$8_$9_t$3_r$4_v$5
mv x$1_y$2_$8_$9_t$3_r$4_v$5_SalidaVisual_test.txt x$1_y$2_$8_$9_t$3_r$4_v$5
mv x$1_y$2_$8_$9_t$3_r$4_v$5_SalidaVisualCal_test.txt x$1_y$2_$8_$9_t$3_r$4_v$5
mv x$1_y$2_$8_$9_t$3_r$4_v$5_SalidaVisual_train.txt x$1_y$2_$8_$9_t$3_r$4_v$5
mv x$1_y$2_$8_$9_t$3_r$4_v$5_SalidaVisualCal_train.txt x$1_y$2_$8_$9_t$3_r$4_v$5
mv proyeccion.sam x$1_y$2_$8_$9_t$3_r$4_v$5
mv proyeccion_sa.ps x$1_y$2_$8_$9_t$3_r$4_v$5
mv grMapa.ps x$1_y$2_$8_$9_t$3_r$4_v$5
mv grMapaCal.ps x$1_y$2_$8_$9_t$3_r$4_v$5
mv x$1_y$2_$8_$9_t$3_r$4_v$5_trazaQError_test.txt x$1_y$2_$8_$9_t$3_r$4_v$5
mv x$1_y$2_$8_$9_t$3_r$4_v$5_trazaQError_train.txt x$1_y$2_$8_$9_t$3_r$4_v$5
