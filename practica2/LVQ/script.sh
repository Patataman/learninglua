#
# $1 es el archivo de prototipos de entrada
#
# $2 es el archivo de prototipos de salida
#
# $3 es el fichero salida con la clasificación
#
# $4 nº de prototipos
#
# $5 número de iteraciones
#
#
/opt/lvq_pak-3.1/eveninit -din trainNormalizado.dat -cout $1.cod -noc $4
/opt/lvq_pak-3.1/mindist -cin $1.cod
/opt/lvq_pak-3.1/balance -din trainNormalizado.dat -cin $1.cod -cout $1.cod
/opt/lvq_pak-3.1/olvq1 -din trainNormalizado.dat -cin $1.cod -cout $2 -rlen $5
/opt/lvq_pak-3.1/accuracy -din testNormalizado.dat -cin $2 > $4_$5_accuracy_test
/opt/lvq_pak-3.1/accuracy -din trainNormalizado.dat -cin $2 > $4_$5_accuracy_train
/opt/lvq_pak-3.1/classify -din testNormalizado.dat -cin $2 -dout $3_test
/opt/lvq_pak-3.1/classify -din trainNormalizado.dat -cin $2 -dout $3_train
/opt/lvq_pak-3.1/sammon -cin $1.cod -cout $4_$5_codebook.sam -rlen 100 -ps

mkdir $4_$5
mv $1.cod $4_$5
mv $2 $4_$5
mv $3_test $4_$5
mv $3_train $4_$5
mv $1.lra $4_$5
mv $4_$5_accuracy_test $4_$5
mv $4_$5_accuracy_train $4_$5
mv $4_$5_codebook.sam $4_$5
mv $4_$5_codebook_sa.ps $4_$5
