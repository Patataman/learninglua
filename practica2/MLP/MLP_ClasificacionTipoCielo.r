#install.packages("RSNNS")

library(RSNNS)   #carga en memoria el paquete RSNNS

###############################################################
## LEER FICHEROS de datos  ####################################
## deben estar separados en entranamiento y test y normalizados
###############################################################

train <- read.csv(file="trainNormalizado.dat", header=TRUE)
test <- read.csv(file="testNormalizado.dat", header=TRUE)

numcol<-ncol(train)


#separar entrada y salida

inputs.train<-train[,1:(numcol-1)]
targets.train<-train[,numcol]

inputs.test<-test[,1:(numcol-1)]
targets.test<-test[,numcol]


# TRANSFORMAR SALIDAS para MLP
# genera una matriz binaria para las salidas, tantas columnas como clases
#########################
#Clases: 1: "cieloDespejado", 2: "multinube", 3:"nube" 

targets.train.MLP <- decodeClassLabels(targets.train)
targets.test.MLP  <- decodeClassLabels(targets.test)

# transformar las entradas de dataframe a matrix para mlp: 
inputs.train <- as.matrix(inputs.train)
inputs.test  <- as.matrix(inputs.test)

######################################################################################
######################### ENTRENAR EL MODELO MLP ##########################################
######################################################################################

modelMlp <- mlp(x=inputs.train,                     ## datos de entrada de entrenamiento
                y=targets.train.MLP,                ## salida esperada de entrenamiento
                size=100,                            ## num de neuronas ocultas. m?s capas ej: size=c(20,20)
                learnFuncParams=c(0.05),            ## raz?n de aprendizaje
                maxit=5000,                         ## num de ciclos
                inputsTest=inputs.test,             ## entrada de validacion o de test
                targetsTest=targets.test.MLP)       ## salida esperada de validacion o de test


#############################################################################################
## Plot de evoluci?n del error
#############################################################################################
#par(mfrow=c(1,1))
plotIterativeError(modelMlp)


#salida real del modelo con datos de entrenamiento y de test
outputs.train.MLP<-predict(modelMlp,inputs.train)   #se le pasa a la funci?n predict el modelo y las entradas
outputs.test.MLP<-predict(modelMlp,inputs.test)

# calcular errores
SSE_train <-sum((targets.train.MLP - outputs.train.MLP)^2)
SSE_test <-sum((targets.test.MLP - outputs.test.MLP)^2)

MSE_train <- SSE_train/nrow(train)
MSE_test <- SSE_test/nrow(test)

####calcular la clase de salida
#transforma las tres columnas reales en la clase 1,2,3, seg?n el m?ximo de los tres valores. Transforma el entero 1,2,3 en factor
outputs.train.class<-as.factor(apply(outputs.train.MLP,1,which.max))  
outputs.test.class<-as.factor(apply(outputs.test.MLP,1,which.max)) 


## ver matriz de confusi?n de train y de test
cM.train <- confusionMatrix(targets.train.MLP,outputs.train.MLP)
cM.test <- confusionMatrix(targets.test.MLP,outputs.test.MLP)
print(cM.train)
print(cM.test)

######################################################################################
######################### GUARDAR RESULTADOS##########################################
######################################################################################

# escribir en fichero la evoluci?n de los errores SSE para entrenamiento y validaci?n

write.table(modelMlp$IterativeFitError,file="EvolErrorTrain.txt",row.names = TRUE,col.names=FALSE)
write.table(modelMlp$IterativeTestError,file="EvolErrorTest.txt",row.names = TRUE,col.names=FALSE)

# salidas reales de la red para train y test (tres columnas, una por cada neurona de salida)
write.table(outputs.train.MLP,file="SalidasRealesTrain.txt",row.names = TRUE ,col.names=FALSE)
write.table(outputs.test.MLP,file="SalidasRealesTest.txt",row.names = TRUE,col.names=FALSE)

# salidas con la clase  de la red para train y test
write.table(outputs.train.class,file="SalidasClaseTrain.txt",row.names = TRUE ,col.names=FALSE)
write.table(outputs.test.class,file="SalidasClaseTest.txt",row.names = TRUE,col.names=FALSE)

#matrices de confusi?n
write.table(cM.train,file="MatrizConfTrain.txt",row.names = TRUE ,col.names=FALSE)
write.table(cM.test,file="MatrizConfTest.txt",row.names = TRUE,col.names=FALSE)



# guardar red entrenada en un archivo binario tipo RDS
# si se quiere recuperar el modelo usar model <- readRDS("RedEntrenada.rds")

saveRDS(modelMlp,"RedEntrenada.rds")

# guardar errores SSE y MSE 
# guardar valores en matriz y escribir en fichero
errores <- matrix(4,2,2)
colnames(errores)<- c("SSE","MSE")
rownames(errores)<- c("Train","Test")
errores["Train","SSE"]<-SSE_train
errores["Train","MSE"]<-MSE_train
errores["Test","SSE"]<-SSE_test
errores["Test","MSE"]<-MSE_test
print(errores)

write.table(errores,file="SSE_MSE.txt",row.names = TRUE ,col.names=TRUE)

